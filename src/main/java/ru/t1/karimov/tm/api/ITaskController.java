package ru.t1.karimov.tm.api;

public interface ITaskController {

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void showTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void clearTasks();

}
