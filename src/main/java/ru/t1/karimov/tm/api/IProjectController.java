package ru.t1.karimov.tm.api;

public interface IProjectController {

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void showProjects();

}
